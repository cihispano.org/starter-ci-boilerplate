<img style="float: right;" width="500px" alt="Logo CiHispano" src="https://www.cihispano.org/assets/img/logo-cihispano.png" />

# Starter-ci-boilerplate

### Framework Codeigniter 4

[![Versión](https://img.shields.io/badge/Versión-2.1.0-red.svg)](https://gitlab.com/cihispano.org/starter-ci-boilerplate/-/tags/v2.1.0) [![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) [![Love Angular badge](https://img.shields.io/badge/CodeIgniter-Love-red?logo=codeigniter)](https://t.me/CodeIgniterEnEspanol)

## Introducción

Este es un starter de desarrollo con Codeigniter y Webpack 5 usando Babel, PostCss y Sass

## ¿Qué es CodeIgniter?

CodeIgniter es un marco web PHP de pila completa que es ligero, rápido, flexible y seguro.

- Sitio oficial: [Sitio oficial](http://codeigniter.com).
- Repositorio oficial: [Repositorio](https://github.com/codeigniter4/CodeIgniter4).
- Guía de usuario oficial: [Aquí](https://codeigniter4.github.io/userguide/).

## Enlaces rápidos

- [Starter-ci-boilerplate](#starter-ci-boilerplate)
    - [Framework Codeigniter 4](#framework-codeigniter-4)
  - [Introducción](#introducción)
  - [¿Qué es CodeIgniter?](#qué-es-codeigniter)
  - [Enlaces rápidos](#enlaces-rápidos)
  - [Comenzando 🚀](#comenzando-)
    - [Pre-requisitos 📋](#pre-requisitos-)
      - [Requerimientos del Servidor 🧾](#requerimientos-del-servidor-)
  - [Instalación 🔧](#instalación-)
      - [Instalación por Git 😺](#instalación-por-git-)
  - [Nota de Actualización ⚡](#nota-de-actualización-)
  - [Despliegue 📦](#despliegue-)
    - [Configuración de Variables de Entorno 📄](#configuración-de-variables-de-entorno-)
    - [Actualización](#actualización)
    - [Configuración para desarrollo](#configuración-para-desarrollo)
    - [Comandos npm 📦](#comandos-npm-)
    - [Configuración ⚙](#configuración-)
    - [Qué incluye el Starter 📦](#qué-incluye-el-starter-)
  - [Construido con 🛠️](#construido-con-️)
  - [Contribuyendo 🖇️](#contribuyendo-️)
  - [Wiki 📖](#wiki-)
  - [Versionado 📌](#versionado-)
  - [Autores ✒️](#autores-️)
  - [Comunidad 👥](#comunidad-)
  - [Expresiones de Gratitud 🎁](#expresiones-de-gratitud-)

<a name="comenzando"></a>
## Comenzando 🚀

Mira **Programador** para conocer como desplegar el starter.

<a name="pre-requisitos"></a>
### Pre-requisitos 📋

Para poder desplegar este starter se requiere de lo siguiente para que pueda trabajar correctamente

- NodeJS 14.19.3
- Composer 2
- Servidor Apache / NGINX (Opcional)
- Servidor MySQL (Opcional)
- Servidor PostgreSQL (Opcional)
- Docker  (Opcional)
- PHP 8.1

Los requisitos marcados como opcionales son porque el starter se puede desplegar en Docker, y el starter ya tiene una configuración para poder implementar tu proyecto dentro de un contenedor

<a name="requerimientos-del-servidor"></a>
#### Requerimientos del Servidor 🧾

De igual manera como alternativas puedes usar alguno de estos programas para poder montar un servidor web de forma rápida y sencilla con MySQL y PHP 7.0+

- [XAMPP (Windows, Mac)](https://www.apachefriends.org/es/index.html)
- [Laragon (Linux, Windows, Mac)](https://laragon.org/)
- Etc, etc.

Se requiere las siguientes extensiones instaladas y habilitadas en su PHP:

- [json](http://php.net/manual/en/json.installation.php)
- [mbstring](http://php.net/manual/en/mbstring.installation.php)
- [intl](http://php.net/manual/en/intl.installation.php)

Extensiones PHP opcionales:

Para más información de las extensiones opciones, [ver documentación oficial](https://codeigniter.com/user_guide/intro/requirements.html#id2)

<a name="instalacion"></a>
## Instalación 🔧

Para comenzar con la instalación y despliegue del proyecto puedes descargar la 
[versión 2.1.0](https://gitlab.com/cihispano.org/starter-ci-boilerplate/-/releases/v2.1.0) desde los tags, esta versión 
está lista para producción.

<a name="instalacion-por-git"></a>
#### Instalación por Git 😺

Para clonar el proyecto por medio del comando git, puedes escoger SSH o HTTPS

```bash
# Clona el repo desde tu terminal por medio SSH:
git clone git@gitlab.com:cihispano.org/starter-ci-boilerplate.git
```

```bash
# Clona el repo desde tu terminal por medio HTTPS:
git clone https://gitlab.com/cihispano.org/starter-ci-boilerplate.git
```

<a name="nota-actualizaciones"></a>
## Nota de Actualización ⚡

> Importante a partir de la versión 2.1.0 del respositorio se debe eliminar la carpeta ./system debido a que la nueva funcionalidad
> instala y actualiza el core del Framework desde Composer.


> Debido al cambio en la configuración de docker, debe cambiarse la notación del fichero .env para las variables con
> espacio de nombre para utilizar la notación _ en vez de la notación. Tal como indica en la propia documentación 
> oficial de CodeIgniter.

<a name="despliegue"></a>
## Despliegue 📦

_Para poder realizar el despliegue correcto del starter se puede hacer lo siguiente_

<a name="docker"></a>
### Docker 🐳

<a name="creacion-de-las-imagenes-de-docker"></a>
#### Creación de las imágenes de docker 🐋

> Puede ver una lista completa de las opciones con el comando `make help`

Para crear las imágenes, utilice el comando **make**.

```bash
make build
```

Iniciar los contenedores con el comando

```bash
make start
```

Y entrar en el contenedor de backend para poder ejecutar el resto de los comandos.

```bash
make ssh-be
```

Instalación de dependencias de PHP

```bash
composer install
```

Con el siguiente comando se puede generar una nueva llave de cifrado y la escribe en el archivo **.env**

```bash
php spark key:generate
```

Seguido instale las dependencias de Node

```bash
npm install
```

Para compilar los recursos ejecute

```bash
npm run dev
```

<a name="configuracion-de-variables-de-entorno"></a>
### Configuración de Variables de Entorno 📄

Para poder realizar la configuración correcta de las variables de entorno se debe realizar una copia del fichero **env**
y renombrarlo a **.env**, y así poder realizar la configuración según sus necesidades.

Un ejemplo de configuración para el entorno de desarrollo utilizando tanto **docker como php spark serve** puede ser el
siguiente:

```ruby
#--------------------------------------------------------------------
# ENVIRONMENT
#--------------------------------------------------------------------
CI_ENVIRONMENT=development

#--------------------------------------------------------------------
# APP
#--------------------------------------------------------------------
app_baseURL=https://localhost/
app_indexPage=
````

<a name="actualizacion"></a>
### Actualización

Para actualizar las dependencias de PHP y Node ejecute (desde el contenedor de backend)

```bash
composer update && npm update && npm run dev
```

<a name="comandos-npm"></a>
### Configuración para desarrollo

Puede configurar git con un hook de pre-commit para que realize las siguientes comprobaciones:

- Comprobación de que el código no tiene errores de sintaxis.
- Comprobación del código con php-cs-fixer.
- Comprobación del código con phpstan.

En caso de que se detecte algún fallo se anulará el commit indicando los errores encontrados.

Se puede ejecutar tanto desde el contenedor o desde el host de desarrollo (en ese caso la comprobación de los
errores de sintaxis los realizará con la versión de PHP instalada en dicha máquina).

```shell
composer install-git-hooks
```

### Comandos npm 📦

Los comandos disponibles en el repositorio son los siguientes

- npm run dev
- npm run watch
- npm run server

<a name="configuracion"></a>
### Configuración ⚙

- Renombrar él `env` por `.env`
- Definir la variable `CI_ENVIRONMENT` a `development`
- Definir él `app.baseURL` al nombre de tu proyecto. Ejemplo 'http://localhost/starter-ci-boilerplate/public'. En caso 
de utilizar __Docker__, debe asignarle el valor https://localhost.

<a name="que-incluye-el-starter"></a>
### Qué incluye el Starter 📦

Dentro de la descarga, encontrará la siguiente estructura de carpetas, que agrupan lógicamente los activos comunes y 
brindan variaciones tanto compiladas como minimizadas. Verás algo como esto:

```
├── app
│   ├── Config
│   │   └── Boot
│   ├── Controllers
│   ├── Database
│   │   ├── Migrations
│   │   └── Seeds
│   ├── Filters
│   ├── Helpers
│   ├── Language
│   ├── Libraries
│   ├── Models
│   ├── ThirdParty
│   └── Views
│       ├── auth
│       ├── errors
│       ├── layouts
│       └── partials
├── docker
│   ├── Back
│   ├── Front
│   └── MariaDB
├── public
├── src
│   ├── img
│   ├── js
│   └── scss
├── tests
└── writable
```

<a name="construido-con"></a>
## Construido con 🛠️

_Utilizamos las siguientes herramientas para desarrollar este proyecto_

- [Codeigniter](https://codeigniter.com/)
- [Babel](https://babeljs.io/)
- [CoreUI](https://coreui.io/)
- [Docker](https://www.docker.com/)
- [Fontawesome](https://fontawesome.com/)
- [PostCSS](https://postcss.org/)
- [Sass](https://sass-lang.com/)
- [Vue](https://vuejs.org/)
- [webpack](https://webpack.js.org/)

<a name="contribuyendo"></a>
## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gitlab.com/cihispano.org/starter-ci-boilerplate/-/blob/master/CONTRIBUTING.md) 
para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

<a name="wiki"></a>
## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra
[Wiki](https://gitlab.com/cihispano.org/starter-ci-boilerplate/-/wikis/home)

<a name="versionado"></a>
## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los 
[tags en este repositorio](https://gitlab.com/cihispano.org/starter-ci-boilerplate/-/tags).

<a name="autores"></a>
## Autores ✒️

_En este colaboran los siguientes profesionales_

- **Jorge armando Pacheco Del real** - [MisterGeorge](https://gitlab.com/MisterGeorge)
- **Jesús Guerreiro Real de Asua** - [dasua](https://gitlab.com/dasua)
- **Ramon Bastardo** - [racede](https://gitlab.com/racede)
- **Manuel Gil** - [manuel.gil](https://gitlab.com/manuel.gil)
- **Miguel Martinez** - [djmai](https://github.com/djmai)

También puedes mirar la lista de todos los 
[contribuyentes](https://gitlab.com/cihispano.org/starter-ci-boilerplate/-/blob/master/CONTRIBUTING.md) quienes han 
participado en este proyecto.

<a name="comunidad"></a>
## Comunidad 👥

Únase a la conversación y ayude a la comunidad.

- [Discord](https://discord.gg/npqRDz5WjR)
- [GitHub](https://github.com/cihispano)
- [GitLab](https://gitlab.com/cihispano.org/)
- [Telegram](https://t.me/CodeIgniterEnEspanol)
- [Twitter](https://twitter.com/CiHispano)
- [YouTube](https://www.youtube.com/channel/UCGrxV4uPJ0rCpSyOI-BlZTw)


[![Love Angular badge](https://img.shields.io/badge/CodeIgniter-Love-red?logo=codeigniter)](https://gitlab.com/cihispano.org)

<a name="expresiones-de-gratitud"></a>
## Expresiones de Gratitud 🎁

- Invita una cerveza 🍺 o un café ☕ a alguien del equipo.
- Da las gracias públicamente 🤓.
- etc.

---

⌨️ con ❤️ por [CiHispano](https://gitlab.com/cihispano.org) 😊
