/**
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * Copyright (c) 2019-2022 CiHispano Org
 *
 * @package    Starter-ci-boilerplate
 * @author     CiHispano Dev Team
 * @copyright  CiHispano <administracion@cihispano.org>
 * @license    For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 * @link       https://cihispano.org/
 * @since      Version 2.0.5
 * @filesource
 * 
 */
const path    = require('path')
const webpack = require('webpack')
const os      = require('os')

// Plugins
const isDevelopment          = process.env.CI_ENVIRONMENT === 'development'
const MiniCssExtractPlugin   = require('mini-css-extract-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const { VueLoaderPlugin }    = require('vue-loader')
const JsonMinimizerPlugin    = require('json-minimizer-webpack-plugin')
const CopyPlugin             = require('copy-webpack-plugin')

module.exports = {
    // "production" | "development" | "none"
    mode : 'development',
    entry: {
        app  : path.resolve(__dirname, 'src/js/app'),
        dash : path.resolve(__dirname, 'src/js/dashboard/index'),
        auth : path.resolve(__dirname, 'src/js/auth/index')
    },
    output: {
        clean    : true,
        path     : path.resolve(__dirname, 'public/dist/'),
        filename : 'js/[name].js',
        assetModuleFilename: 'img/[name][ext]'
    },
    resolve: {
        modules: [
            "node_modules",
            path.resolve(__dirname, 'public/dist/')
        ],
        // directories where to look for modules
        extensions: [
            '.js', '.json', '.jsx',
            '.scss', '.sass', '.css',
            '.gif', '.png', '.jpg', '.jpeg', '.svg',
            '.vue'
        ],
        alias: {
            vue: 'vue/dist/vue.js',
        }
    },
    module: {
        rules: [
            {
                test   : /\.(t|j)sx?$/,
                loader : 'babel-loader',
                exclude: /(node_modules|bower_components)/
            },
            {
                test   : /\.vue$/,
                loader : 'vue-loader'
            },
            // CSS, Scss and Sass
            {
                test: /\.s[ac]ss$/i,
				use: [
                    isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        }
                    },
					{
						// Run postcss actions
						loader: 'postcss-loader',
						options: {
						  	// `postcssOptions` is needed for postcss 8.x;
						  	// if you use postcss 7.x skip the key
							postcssOptions: {
								// postcss plugins, can be exported to postcss.config.js
								plugins: function () {
								return [
									require('autoprefixer')
								];
								}
							}
						}
					},
					{
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                            sassOptions: {
                                outputStyle: 'compressed'
                            }
                        }
                    },
				]
			},
            // Images
            {
                test: /\.(?:ico|gif|png|jpg|jpeg|webp|tiff)$/i,
                type: 'asset/resource',
            },
            // Fonts
            {
                test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
                type: 'asset/inline',
            },
            // SVGs files
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/'
                        }
                    }
                ]
            },
            // Json files
            {
                test: /\.json$/i,
                type: "asset/resource",
            }
        ]
    },
    devtool: 'source-map',
    plugins: [
        // Only update what has changed on hot reload
        new webpack.HotModuleReplacementPlugin(),
        new VueLoaderPlugin(),
        new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),
        new MiniCssExtractPlugin({
            filename: isDevelopment ? 'src/scss/[name].scss' : 'css/[name].css',
            chunkFilename: isDevelopment ? 'src/scss/[id].scss' : 'css/[name].css'
        }),
        // Configuración para los archivos Json generados para los Favicon
        new CopyPlugin({
            patterns: [
                {
                    from: os.platform() == 'win32' ? 'src/img/favicon/manifest.json' : path.resolve(__dirname, 'src/img/favicon/*.json'),
                    to({ context, absoluteFilename }) {
                        return Promise.resolve(path.resolve(__dirname, 'public/dist/img/[name][ext]'));
                    }
                }
            ]
        }),
        // Provide Popper.js dependencies
        new webpack.ProvidePlugin({
            Popper: ['popper.js', 'default']
        })
    ],
    optimization: {
        minimize   : true,
        minimizer: [
            // For webpack@5 you can use the `...` syntax to extend existing minimizers (i.e. `terser-webpack-plugin`), uncomment the next line
            // `...`
            new JsonMinimizerPlugin(),
        ],
        splitChunks : {
            chunks  : 'all',
            minSize : 0,
            name    : 'commons'
        }
    }
}
