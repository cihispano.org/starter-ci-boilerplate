# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.5] - 2022-10-03

### Changed

-   Actualización de CHANGELOG
-   Modificado composer.json para utilizar el framework instalado con composer

### Added

-   Se agrega la librería cihispano/breadcrumbs de autoría de la comunidad para el uso de los Breadcrumbs
-   Agragado el texto de licencia para derechos de autor de este repositorio

### Removed

- Removida la librería geeklabs/ci4-breadcrumbs 

## [2.0.4] - 2022-09-29

### Added

-   Se agrega scaffolding módulo Permisos
-   Se agrega scaffolding módulo Roles
-   Se agrega scaffolding módulo Usuarios
-   Se agrega funcionalidad para los Breadcrumbs
-   Se configura el PHPStan
-   Se configura el PHP Coding Standards Fixers

### Changed

-   Actualización del README
-   Se optimiza el compilado de recursos
-   Se optimiza la configuración del Docker

### Fixed

-   Correción de rutas.
-   Correción del utorouting.

## [2.0.3] - 2022-09-21

### Added

-   Se actualiza el Core de CI de la versión 4.1.9 a la 4.2.6
-   Actualización HTML en los componentes

### Removed

-   Se elimina vista original de Welcome

### Fixed

-   Corrección en la configuración SCSS

## [2.0.2] - 2022-05-24

### Added

-   Se agrega el resolve para el PATH de acuerdo al Sistema operativo, especialmente al Windows x86.

### Removed

-   Se elimina jQuery como dependencia para el compilado de los recursos.

### Fixed

-   Se corrige el error del PATH en el archivo del archivo .json del favicon

## [2.0.1] - 2022-05-18

### Changed

-   Se actualiza CodeIgniter 4.1.9
-   Se actualiza Vue 2.6.14
-   Se actualiza FontAwesome 6.1.1
-   Se actualiza Webpack 5.72.0
-   Se actualiza Coreui 4.1.5

## [2.0.0] - 2022-05-18

### Changed

-   Se optimiza la configuración de Webpack.
-   Se optimizan los layouts, los parciales y componentes del repositorio.
-   Se actualizan las dependencias fundamentales para el funcionamiento del repositorio.

[unreleased]: https://gitlab.com/cihispano.org/starter-ci-boilerplate/-/compare/v2.0.5...develop?from_project_id=21276763&straight=true
[2.0.5]: https://gitlab.com/cihispano.org/starter-ci-boilerplate/-/compare/v2.0.4...v2.0.5?from_project_id=21276763&straight=true
[2.0.4]: https://gitlab.com/cihispano.org/starter-ci-boilerplate/-/compare/v2.0.3...v2.0.4?from_project_id=21276763&straight=true
[2.0.3]: https://gitlab.com/cihispano.org/starter-ci-boilerplate/-/compare/v2.0.2...v2.0.3?from_project_id=21276763&straight=true
[2.0.2]: https://gitlab.com/cihispano.org/starter-ci-boilerplate/-/compare/v2.0.1...v2.0.2?from_project_id=21276763&straight=true
[2.0.1]: https://gitlab.com/cihispano.org/starter-ci-boilerplate/-/compare/Nightly...v2.0.1?from_project_id=21276763&straight=true
[2.0.0]: https://gitlab.com/cihispano.org/starter-ci-boilerplate/-/releases/Nightly
