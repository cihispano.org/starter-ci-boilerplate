<?php
/*
 * Copyright (c) 2024.
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * @copyright CiHispano <administracion@cihispano.org>
 * @license For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 *
 */

declare(strict_types=1);

namespace App\Controllers;

final class Permissions extends BaseAdminController
{
    public function index(): string
    {
        $data['breadcrumbs'] = breadcrumb_current_url();

        return view('permissions/index', $data);
    }

    public function add(): string
    {
        $data['breadcrumbs'] = breadcrumb_current_url();

        return view('permissions/add', $data);
    }

    public function edit(int $id): string
    {
        $data = [
            'breadcrumbs' => breadcrumb_current_url(),
            'id'          => $id,
        ];

        return view('permissions/edit', $data);
    }
}
