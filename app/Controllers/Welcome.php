<?php
/*
 * Copyright (c) 2024.
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * @copyright CiHispano <administracion@cihispano.org>
 * @license For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 *
 */

namespace App\Controllers;

class Welcome extends BaseController
{
    public function index(): string
    {
        return view('welcome');
    }
}
