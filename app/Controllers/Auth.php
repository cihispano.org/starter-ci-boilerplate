<?php
/*
 * Copyright (c) 2024.
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * @copyright CiHispano <administracion@cihispano.org>
 * @license For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 *
 */

declare(strict_types=1);

namespace App\Controllers;

use CodeIgniter\HTTP\RedirectResponse;

final class Auth extends BaseController
{
    public function login(): string
    {
        return view('auth/login');
    }

    public function logout(): RedirectResponse
    {
        return redirect()->route('app_welcome_get');
    }

    public function forget(): string
    {
        return view('auth/forget');
    }

    public function register(): string
    {
        return view('auth/register');
    }
}
