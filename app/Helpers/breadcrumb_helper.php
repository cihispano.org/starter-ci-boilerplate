<?php

/*
 * Copyright (c) 2022.
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * @copyright CiHispano <administracion@cihispano.org>
 * @license For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 *
 */

declare(strict_types=1);

use Cihispano\Breadcrumbs\Breadcrumb;
use Cihispano\Breadcrumbs\BreadcrumbConfig;
use Cihispano\Breadcrumbs\BreadcrumbException;

if (! function_exists('breadcrumb_current_url')) {
    /**
     * Render the current url
     */
    function breadcrumb_current_url(?int $maxLevels = null): string
    {
        $config     = config(Config\Breadcrumb::class);
        $uri        = service('uri');
        $breadcrums = new Breadcrumb(new BreadcrumbConfig(
            $config->generateLinks,
            $config->separator,
            $config->navTag,
            $config->olTag,
            $config->liTag
        ));
        $segments = $uri->getSegments();
        $link     = '';

        foreach ($segments as $segment) {
            $link .= $segment . '/';
            $segmentText = ucwords(str_replace('-', ' ', $segment));

            try {
                $breadcrums->addBreadcrumb($segmentText, base_url($link));
            } catch (BreadcrumbException $e) {
                $logger ??= service('logger');
                $logger->alert('Breadcrumb segment error: ' . $e->getMessage(), ['file' => $e->getFile(), 'line' => $e->getLine()]);
            }
        }

        return $breadcrums->render($maxLevels);
    }
}
