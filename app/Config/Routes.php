<?php

use App\Controllers\Admin;
use App\Controllers\Auth;
use App\Controllers\Permissions;
use App\Controllers\Roles;
use App\Controllers\Users;
use App\Controllers\Welcome;
use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', [Welcome::class, 'index'], ['as' => 'app_welcome_get']);

/**
 * --------------------------------------------------------------------
 * Auth Routes
 * --------------------------------------------------------------------
 */
$routes->get('/change', [Auth::class, 'change_password'], ['as' => 'auth_change_password_get']);
$routes->get('/forget', [Auth::class, 'forget'], ['as' => 'auth_forget_get']);
$routes->get('/login', [Auth::class, 'login'], ['as' => 'auth_login_get']);
$routes->get('/logout', [Auth::class, 'logout'], ['as' => 'auth_logout_get']);
$routes->get('/register', [Auth::class, 'register'], ['as' => 'auth_register_get']);

/**
 * --------------------------------------------------------------------
 * Admin Routes
 * --------------------------------------------------------------------
 */
$routes->group('admin', static function ($routes) {
    $routes->get('/', [Admin::class, 'index'], ['as' => 'admin_index_get']);

    /**
     * --------------------------------------------------------------------
     * Permissions  Module Routes
     * --------------------------------------------------------------------
     */
    $routes->get('permissions', [Permissions::class, 'index'], ['as' => 'permissions_index_get']);
    $routes->get('permissions/add', [Permissions::class, 'add'], ['as' => 'permissions_add_get']);
    $routes->get('permissions/edit/(:num)', [Permissions::class, 'edit/$1'], ['as' => 'permissions_edit_get']);

    /**
     * --------------------------------------------------------------------
     * Roles  Module Routes
     * --------------------------------------------------------------------
     */
    $routes->get('roles', [Roles::class, 'index'], ['as' => 'roles_index_get']);
    $routes->get('roles/add', [Roles::class, 'add'], ['as' => 'roles_add_get']);
    $routes->get('roles/edit/(:num)', [Roles::class, 'edit/$1'], ['as' => 'roles_edit_get']);

    /**
     * --------------------------------------------------------------------
     * Users  Module Routes
     * --------------------------------------------------------------------
     */
    $routes->get('users', [Users::class, 'index'], ['as' => 'users_index_get']);
    $routes->get('users/add', [Users::class, 'add'], ['as' => 'users_add_get']);
    $routes->get('users/edit/(:num)', [Users::class, 'edit'], ['as' => 'users_edit_get']);
});
