<?php

declare(strict_types=1);

namespace Config;

use Cihispano\Breadcrumbs\BreadcrumbConfig;
use CodeIgniter\Config\BaseConfig;

/**
 * Copyright (c) 2022.
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * @copyright CiHispano <administracion@cihispano.org>
 *
 * @license For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 */
class Breadcrumb extends BaseConfig
{
    public bool $generateLinks = true;
    public string $separator   = '';
    public string $navTag      = BreadcrumbConfig::DEFAULT_NAV_TAG;
    public string $olTag       = BreadcrumbConfig::DEFAULT_OL_TAG;
    public string $liTag       = BreadcrumbConfig::DEFAULT_LI_TAG;
}
