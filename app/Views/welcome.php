<?= $this->extend('layouts/welcome') ?>

<?= $this->section('title') ?>
	Bienvenido a CodeIgniter 4!
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <div class="container">
        <header class="d-flex flex-wrap align-items-center justify-content-end py-3 mb-4 border-bottom">
            <div class="col-3 text-end">
                <?= anchor('/login', 'Iniciar sesión', 'role="button" class="btn btn-outline-primary me-2"') ?>
                <?= anchor('/register', 'Registrarse', 'role="button" class="btn btn-primary"') ?>
            </div>
        </header>
    </div>
	<div class="container">
		<div class="row">
	 		<div class="col">
				<div class="card shadow">
				  	<div class="card-header">
						<img alt="CodeIgniter" src="<?= base_url('/dist/fonts/ci-logo.svg'); ?>">
				  	</div>
				  	<div class="card-body">
				    	<div class="row">
				    		<div class="col-8">
								<h4 class="card-title">{{ name }} {{ version }}</h4>
				    			<p class="card-text text-justify">
									Bienvenido a <strong>Starter-ci-boilerplate</strong> este repositorio fue creado por <a href="https://cihispano.org/" target="_blank" class="text-decoration-none text-warning">CiHispano</a> somos
									un grupo de profesionales para compartir conocimientos sobre <a href="https://codeigniter.com/" target="_blank" class="text-decoration-none text-warning">CodeIgniter</a>
									un potente y liviano framework de PHP para crear aplicaciones web completas. Cuenta con:
						    		<ul>
						    			<li><i class="fa-regular fa-circle-dot"></i> CodeIgniter <?= CodeIgniter\CodeIgniter::CI_VERSION ?></li>
						    			<li><i class="fa-regular fa-circle-dot"></i> Vue</li>
										<li><i class="fa-regular fa-circle-dot"></i> Bootstrap</li>
						    			<li><i class="fa-regular fa-circle-dot"></i> FontAwesome</li>
						    			<li><i class="fa-regular fa-circle-dot"></i> Webpack</li>
						    			<li><i class="fa-regular fa-circle-dot"></i> Coreui</li>
                                        <li><i class="fa-regular fa-circle-dot"></i> Docker</li>
                                        <li><i class="fa-regular fa-circle-dot"></i> phpMyAdmin</li>
                                        <li><i class="fa-regular fa-circle-dot"></i> MariaDB</li>
                                        <li><i class="fa-regular fa-circle-dot"></i> MySQL</li>
                                        <li><i class="fa-regular fa-circle-dot"></i> PHPStan</li>
                                        <li><i class="fa-regular fa-circle-dot"></i> PHP Coding Standards Fixer</li>
						    		</ul>
						    	</p>
				    		</div>
							<div class="col-4">
								<div class="row align-items-center h-100">
									<img alt="Cihispano" src="<?= base_url('/dist/img/cihispano-logo.png'); ?>" class="w-75 mx-auto">
								</div>
							</div>
				    	</div>
				  	</div>
				  	<div class="card-footer">
				  		<p class="card-text">
				  			Página procesada <strong>{elapsed_time}</strong> segundoss.
				  		</p>
				  	</div>
				</div>
	 		</div>
	 	</div>
	</div>
<?= $this->endSection() ?>
