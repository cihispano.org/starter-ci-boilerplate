<?= $this->extend('layouts/auth') ?>

<?= $this->section('title') ?>
    Inicio de sesión
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <ci-login></ci-login>
<?= $this->endSection() ?>
