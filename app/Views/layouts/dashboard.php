<!--
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * Copyright (c) 2019-2022 CiHispano Org
 *
 * @package    Starter-ci-boilerplate
 * @author     CiHispano Dev Team
 * @copyright  CiHispano <administracion@cihispano.org>
 * @license    For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 * @link       https://cihispano.org/
 * @since      Version 2.0.5
 * @filesource
 * -->
<!doctype html>
<html lang="es" class="h-100">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= $this->include('partials/favicon') ?>
        <title><?= $this->renderSection('title') ?></title>
        <!--  App CSS (Do not remove) -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('/dist/css/dash.css'); ?>">
    </head>
    <body>
        <?= $this->include('partials/sidebar') ?>
        <div class="wrapper d-flex flex-column min-vh-100 bg-light" id="dash">
            <?= $this->include('partials/topbar') ?>
            <div class="body flex-grow-1 px-3">
                <main class="container-fluid">
                    <!-- Contenido y carga dinámica de las vistas -->
                    <?= $this->renderSection('content') ?>
                </main>
            </div>
            <?= $this->include('partials/footer') ?>
        </div>
        <!-- App JS (Do not remove) -->
        <script src="<?= base_url('/dist/js/commons.js'); ?>"></script>
        <script src="<?= base_url('/dist/js/dash.js'); ?>"></script>
    </body>
</html>

