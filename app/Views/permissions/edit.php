<?= $this->extend('layouts/dashboard') ?>

<?= $this->section('title') ?>
    Editar permiso
<?= $this->endSection() ?>

<?= $this->section('breadcrumbs') ?>
    <?= $breadcrumbs; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <ci-permissions-edit></ci-permissions-edit>
<?= $this->endSection() ?>
