<?= $this->extend('layouts/dashboard') ?>

<?= $this->section('title') ?>
    Nuevo permiso
<?= $this->endSection() ?>

<?= $this->section('breadcrumbs') ?>
    <?= $breadcrumbs; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <ci-permissions-add></ci-permissions-add>
<?= $this->endSection() ?>
