<?= $this->extend('layouts/dashboard') ?>

<?= $this->section('title') ?>
    Permisos
<?= $this->endSection() ?>

<?= $this->section('breadcrumbs') ?>
    <?= $breadcrumbs; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <ci-permissions></ci-permissions>
<?= $this->endSection() ?>
