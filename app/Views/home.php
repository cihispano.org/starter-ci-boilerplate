<?= $this->extend('layouts/dashboard') ?>

<?= $this->section('title') ?>
    Inicio
<?= $this->endSection() ?>

<?= $this->section('breadcrumbs') ?>
    <?= $breadcrumbs; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <div class="row">
        <div class="col">
            <ci-widget-card
                count      ="0"
                title      ="Permisos"
                icon       ="fa-solid fa-list-check"
                background ="bg-dark"
            ></ci-widget-card>
        </div>
        <div class="col">
            <ci-widget-card
                count      ="0"
                title      ="Proyectos"
                icon       ="fa-solid fa-briefcase"
                background ="bg-dark"
            ></ci-widget-card>
        </div>
        <div class="col">
            <ci-widget-card
                count      ="0"
                title      ="Roles"
                icon       ="fa-solid fa-user-group"
                background ="bg-dark"
            ></ci-widget-card>
        </div>
        <div class="col">
            <ci-widget-card
                count      ="0"
                title      ="Usuarios"
                icon       ="fa-solid fa-user-large"
                background ="bg-dark"
            ></ci-widget-card>
        </div>
    </div>
<?= $this->endSection() ?>
