<?= $this->extend('layouts/dashboard') ?>

<?= $this->section('title') ?>
    Nuevo usuario
<?= $this->endSection() ?>

<?= $this->section('breadcrumbs') ?>
    <?= $breadcrumbs; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <ci-users-add></ci-users-add>
<?= $this->endSection() ?>
