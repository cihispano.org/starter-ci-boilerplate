<?= $this->extend('layouts/dashboard') ?>

<?= $this->section('title') ?>
    Usuarios
<?= $this->endSection() ?>

<?= $this->section('breadcrumbs') ?>
    <?= $breadcrumbs; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <ci-users></ci-users>
<?= $this->endSection() ?>
