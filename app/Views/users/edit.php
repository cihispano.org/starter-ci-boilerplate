<?= $this->extend('layouts/dashboard') ?>

<?= $this->section('title') ?>
    Editar usuario
<?= $this->endSection() ?>

<?= $this->section('breadcrumbs') ?>
    <?= $breadcrumbs; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <ci-users-edit></ci-users-edit>
<?= $this->endSection() ?>
