<?= $this->extend('layouts/dashboard') ?>

<?= $this->section('title') ?>
    Roles
<?= $this->endSection() ?>

<?= $this->section('breadcrumbs') ?>
    <?= $breadcrumbs; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <ci-roles></ci-roles>
<?= $this->endSection() ?>
