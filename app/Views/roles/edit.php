<?= $this->extend('layouts/dashboard') ?>

<?= $this->section('title') ?>
    Editar rol
<?= $this->endSection() ?>

<?= $this->section('breadcrumbs') ?>
    <?= $breadcrumbs; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <ci-roles-edit></ci-roles-edit>
<?= $this->endSection() ?>
