<?= $this->extend('layouts/dashboard') ?>

<?= $this->section('title') ?>
    Nuevo rol
<?= $this->endSection() ?>

<?= $this->section('breadcrumbs') ?>
    <?= $breadcrumbs; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <ci-roles-add></ci-roles-add>
<?= $this->endSection() ?>
