<div class="sidebar sidebar-dark sidebar-fixed sidebar-narrow-unfoldable" id="sidebar">
    <div class="sidebar-brand d-none d-md-flex">
        <img alt="CodeIgniter" class="sidebar-brand-full" src="<?= base_url('/dist/fonts/ci-logo.svg'); ?>">
        <img alt="CodeIgniter" class="sidebar-brand-narrow" width="40px" height="40px" src="<?= base_url('/dist/fonts/ci-logo-narrow.svg'); ?>">
    </div>
    <ul class="sidebar-nav" data-coreui="navigation" data-simplebar>
        <li class="nav-title">Módulos</li>
        <li class="nav-item">
            <?= anchor('/admin', '<i class="fa-solid fa-house-fire nav-icon"></i> Inicio', 'class="nav-link"') ?>
        </li>
        <li class="nav-title mt-auto">Configuración</li>
        <li class="nav-group">
            <?= anchor('', '<i class="fa-solid fa-list-check nav-icon"></i> Permisos', 'class="nav-link nav-group-toggle"') ?>
            <ul class="nav-group-items">
                <li class="nav-item">
                    <?= anchor('/admin/permissions', '<i class="fa-solid fa-list-ol nav-icon"></i> Permisos', 'class="nav-link"') ?>
                </li>
                <li class="nav-item">
                    <?= anchor('/admin/permissions/add', '<i class="fa-solid fa-plus nav-icon"></i> Crear Nuevo', 'class="nav-link"') ?>
                </li>
            </ul>
        </li>
		<li class="nav-group">
            <?= anchor('', '<i class="fa-solid fa-user-group nav-icon"></i> Roles', 'class="nav-link nav-group-toggle"') ?>
            <ul class="nav-group-items">
                <li class="nav-item">
                    <?= anchor('/admin/roles', '<i class="fa-solid fa-list-ol nav-icon"></i> Roles', 'class="nav-link"') ?>
                </li>
                <li class="nav-item">
                    <?= anchor('/admin/roles/add', '<i class="fa-solid fa-plus nav-icon"></i> Crear Nuevo', 'class="nav-link"') ?>
                </li>
            </ul>
        </li>
		<li class="nav-group">
            <?= anchor('', '<i class="fa-solid fa-user-large nav-icon"></i> Usuarios', 'class="nav-link nav-group-toggle"') ?>
            <ul class="nav-group-items">
                <li class="nav-item">
                    <?= anchor('/admin/users', '<i class="fa-solid fa-list-ol nav-icon"></i> Usuarios', 'class="nav-link"') ?>
                </li>
                <li class="nav-item">
                    <?= anchor('/admin/users/add', '<i class="fa-solid fa-plus nav-icon"></i> Crear Nuevo', 'class="nav-link"') ?>
                </li>
            </ul>
        </li>
    </ul>
    <button class="sidebar-toggler" type="button" data-coreui-toggle="unfoldable"></button>
</div>
