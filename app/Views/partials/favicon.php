<!-- Favicon -->
<link rel="apple-touch-icon" href="<?= base_url('/dist/img/apple-icon-precomposed.png'); ?>">
<link rel="apple-touch-icon" href="<?= base_url('/dist/img/apple-icon.png'); ?>">
<link rel="apple-touch-icon" sizes="57x57" href="<?= base_url('/dist/img/apple-icon-57x57.png'); ?>">
<link rel="apple-touch-icon" sizes="60x60" href="<?= base_url('/dist/img/apple-icon-60x60.png'); ?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?= base_url('/dist/img/apple-icon-72x72.png'); ?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('/dist/img/apple-icon-76x76.png'); ?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?= base_url('/dist/img/apple-icon-114x114.png'); ?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('/dist/img/apple-icon-120x120.png'); ?>">
<link rel="apple-touch-icon" sizes="144x144" href="<?= base_url('/dist/img/apple-icon-144x144.png'); ?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('/dist/img/apple-icon-152x152.png'); ?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('/dist/img/apple-icon-180x180.png'); ?>">
<link rel="icon" type="image/png" sizes="36x36"  href="<?= base_url('/dist/img/android-icon-36x36.png'); ?>">
<link rel="icon" type="image/png" sizes="48x48"  href="<?= base_url('/dist/img/android-icon-48x48.png'); ?>">
<link rel="icon" type="image/png" sizes="72x72"  href="<?= base_url('/dist/img/android-icon-72x72.png'); ?>">
<link rel="icon" type="image/png" sizes="96x96"  href="<?= base_url('/dist/img/android-icon-96x96.png'); ?>">
<link rel="icon" type="image/png" sizes="144x144"  href="<?= base_url('/dist/img/android-icon-144x144.png'); ?>">
<link rel="icon" type="image/png" sizes="192x192"  href="<?= base_url('/dist/img/android-icon-192x192.png'); ?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('/dist/img/favicon-32x32.png'); ?>">
<link rel="icon" type="image/png" sizes="96x96" href="<?= base_url('/dist/img/favicon-96x96.png'); ?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('/dist/img/favicon-16x16.png'); ?>">
<link rel="manifest" href="<?= base_url('/dist/img/manifest.json'); ?>">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?= base_url('/dist/img/ms-icon-70x70.png'); ?>">
<meta name="msapplication-TileImage" content="<?= base_url('/dist/img/ms-icon-144x144.png'); ?>">
<meta name="msapplication-TileImage" content="<?= base_url('/dist/img/ms-icon-150x150.png'); ?>">
<meta name="msapplication-TileImage" content="<?= base_url('/dist/img/ms-icon-310x310.png'); ?>">
<meta name="theme-color" content="#ffffff">