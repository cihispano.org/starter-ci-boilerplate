<!-- Footer -->
<footer class="footer">
    <div>
        <span>{{ name }} <i class="fa-solid fa-code-branch"></i> {{ version }}. </span>
        Hecho con <i class="fas fa-heart text-warning"></i> por <a href="https://cihispano.org/" target="_blank">CiHispano</a>
    </div>
    <div class="ml-auto">Potenciado por <a href="https://coreui.io/" target="_blank">CoreUI</a></div>
</footer>
