<header class="bg-white p-3 border-bottom mb-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-11 align-self-center">
                <?= $this->renderSection('breadcrumbs') ?>
            </div>
            <div class="col-1">
                <ul class="header-nav justify-content-end my-1">
                    <li class="nav-item dropdown">
                        <?= anchor('', '<i class="fa-solid fa-user-gear fa-lg"></i>', 'class="nav-link py-0" data-coreui-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"') ?>
                        <div class="dropdown-menu dropdown-menu-end pt-0 mt-4">
                            <div class="dropdown-header bg-light text-center py-2">Configuración</div>
                            <div class="dropdown-divider mt-0"></div>
                            <?= anchor('', '<i class="fa-solid fa-envelope me-2"></i> Mensajes<span class="badge badge-sm bg-success ms-2">42</span>', 'class="dropdown-item"') ?>
                            <div class="dropdown-divider"></div>
                            <?= anchor('', '<i class="fa-solid fa-briefcase me-2"></i> Projectos<span class="badge badge-sm bg-primary ms-2">42</span>', 'class="dropdown-item"') ?>
                            <div class="dropdown-divider"></div>
                            <?= anchor('/login', '<i class="fa-solid fa-door-open me-2"></i>Cerrar sesión', 'class="dropdown-item"') ?>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>
