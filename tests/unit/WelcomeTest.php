<?php

namespace Tests\Unit;

use App\Controllers\Welcome;
use CodeIgniter\Test\CIUnitTestCase;

/**
 * @internal
 */
final class WelcomeTest extends CIUnitTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testControllerExists()
    {
        $this->assertTrue(class_exists(Welcome::class), 'The controller class ' . Welcome::class . 'does not exist');
    }

    public function testViewExists()
    {
        $view = APPPATH . 'Views/welcome.php';

        $this->assertFileExists($view, 'The view file 1view does not exist');
    }
}
