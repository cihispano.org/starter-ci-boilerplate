<?php
/*
 * Copyright (c) 2024.
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * @copyright CiHispano <administracion@cihispano.org>
 * @license For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 *
 */

namespace Tests\Unit;

use App\Controllers\Users;
use CodeIgniter\Test\CIUnitTestCase;
use CodeIgniter\Test\FeatureTestTrait;

/**
 * @internal
 */
final class UsersTest extends CIUnitTestCase
{
    use FeatureTestTrait;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testControllerExists()
    {
        $this->assertTrue(class_exists(Users::class), 'The controller class ' . Users::class . 'does not exist');
    }

    public static function provideViewExists(): iterable
    {
        return [
            ['index'],
            ['add'],
            ['edit'],
        ];
    }

    /**
     * testViewExists
     *
     * @param mixed $viewName
     *
     * @return void
     */
    public function testViewExists($viewName)
    {
        $viewPath = APPPATH . 'Views/users/' . $viewName . '.php';

        $this->assertFileExists($viewPath, 'The view file 1viewPath does not exist');
    }

    public function testIndexMethod()
    {
        $route = route_to('users_index_get');

        $result = $this->call('get', $route);
        $result->assertStatus(200);
        $result->assertSee('Usuarios');

        $this->assertTrue($result->isOK());
    }

    public function testAddMethod()
    {
        $route = route_to('users_add_get');

        $result = $this->call('get', $route);
        $result->assertStatus(200);
        $result->assertSee('Nuevo usuario');

        $this->assertTrue($result->isOK());
    }

    public function testEditMethod()
    {
        $id    = 1;
        $route = route_to('users_edit_get', $id);

        $result = $this->call('get', $route);
        $result->assertStatus(200);
        $result->assertSee('Editar usuario');

        $this->assertTrue($result->isOK());
    }
}
