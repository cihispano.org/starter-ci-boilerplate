<?php
/*
 * Copyright (c) 2024.
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * @copyright CiHispano <administracion@cihispano.org>
 * @license For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 *
 */

namespace Tests\Unit;

use App\Controllers\Admin;
use CodeIgniter\Test\CIUnitTestCase;
use CodeIgniter\Test\FeatureTestTrait;

/**
 * @internal
 */
final class AdminTest extends CIUnitTestCase
{
    use FeatureTestTrait;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testControllerExists()
    {
        $this->assertTrue(class_exists(Admin::class), 'The controller class ' . Admin::class . 'does not exist');
    }

    public function testHomeExists()
    {
        $view = APPPATH . 'Views/home.php';

        $this->assertFileExists($view, 'The view file 1view does not exist');
    }

    public static function providePartialsExists(): iterable
    {
        return [
            ['breadcrumbs'],
            ['favicon'],
            ['footer'],
            ['sidebar'],
            ['topbar'],
        ];
    }

    /**
     * testPartialsExists
     *
     * @param mixed $viewName
     *
     * @return void
     */
    public function testPartialsExists($viewName)
    {
        $partialPath = APPPATH . 'Views/partials/' . $viewName . '.php';

        $this->assertFileExists($partialPath, 'The view file 1partialPath does not exist');
    }

    public function testIndexMethod()
    {
        $route = route_to('admin_index_get');

        $result = $this->call('get', $route);
        $result->assertStatus(200);
        $result->assertSee('Inicio');

        $this->assertTrue($result->isOK());
    }
}
