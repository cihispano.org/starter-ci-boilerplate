<?php
/*
 * Copyright (c) 2024.
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * @copyright CiHispano <administracion@cihispano.org>
 * @license For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 *
 */

namespace Tests\Unit;

use App\Controllers\BaseAdminController;
use CodeIgniter\Test\CIUnitTestCase;

/**
 * @internal
 */
final class BaseAdminTest extends CIUnitTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testControllerExists()
    {
        $this->assertTrue(class_exists(BaseAdminController::class), 'The controller class ' . BaseAdminController::class . 'does not exist');
    }
}
