<?php
/*
 * Copyright (c) 2024.
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * @copyright CiHispano <administracion@cihispano.org>
 * @license For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 *
 */

namespace Tests\Unit;

use App\Controllers\Auth;
use CodeIgniter\Test\CIUnitTestCase;
use CodeIgniter\Test\FeatureTestTrait;

/**
 * @internal
 */
final class AuthTest extends CIUnitTestCase
{
    use FeatureTestTrait;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testControllerExists()
    {
        $this->assertTrue(class_exists(Auth::class), 'The controller class ' . Auth::class . 'does not exist');
    }

    public static function provideViewExists(): iterable
    {
        return [
            ['forget'],
            ['login'],
            ['register'],
        ];
    }

    /**
     * testViewExists
     *
     * @param mixed $viewName
     *
     * @return void
     */
    public function testViewExists($viewName)
    {
        $viewPath = APPPATH . 'Views/auth/' . $viewName . '.php';

        $this->assertFileExists($viewPath, 'The view file 1viewPath does not exist');
    }

    public function testLoginMethod()
    {
        $route = route_to('auth_login_get');

        $result = $this->call('get', $route);
        $result->assertStatus(200);
        $result->assertSee('Inicio de sesión');

        $this->assertTrue($result->isOK());
    }

    public function testLogoutMethod()
    {
        $route = route_to('auth_logout_get');

        $result = $this->call('get', $route);
        $result->assertStatus(302);

        $this->assertTrue($result->isOK());
    }

    public function testForgetMethod()
    {
        $route = route_to('auth_forget_get');

        $result = $this->call('get', $route);
        $result->assertStatus(200);
        $result->assertSee('Recuperar clave');

        $this->assertTrue($result->isOK());
    }

    public function testRegisterMethod()
    {
        $route = route_to('auth_register_get');

        $result = $this->call('get', $route);
        $result->assertStatus(200);
        $result->assertSee('Registro');

        $this->assertTrue($result->isOK());
    }
}
