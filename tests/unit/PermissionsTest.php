<?php
/*
 * Copyright (c) 2024.
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * @copyright CiHispano <administracion@cihispano.org>
 * @license For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 *
 */

namespace Tests\Unit;

use App\Controllers\Permissions;
use CodeIgniter\Test\CIUnitTestCase;
use CodeIgniter\Test\FeatureTestTrait;

/**
 * @internal
 */
final class PermissionsTest extends CIUnitTestCase
{
    use FeatureTestTrait;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testControllerExists()
    {
        $this->assertTrue(class_exists(Permissions::class), 'The controller class ' . Permissions::class . 'does not exist');
    }

    public static function provideViewExists(): iterable
    {
        return [
            ['index'],
            ['add'],
            ['edit'],
        ];
    }

    /**
     * testViewExists
     *
     * @param mixed $viewName
     *
     * @return void
     */
    public function testViewExists($viewName)
    {
        $viewPath = APPPATH . 'Views/permissions/' . $viewName . '.php';

        $this->assertFileExists($viewPath, 'The view file 1viewPath does not exist');
    }

    public function testIndexMethod()
    {
        $route = route_to('permissions_index_get');

        $result = $this->call('get', $route);
        $result->assertStatus(200);
        $result->assertSee('Permisos');

        $this->assertTrue($result->isOK());
    }

    public function testAddMethod()
    {
        $route = route_to('permissions_add_get');

        $result = $this->call('get', $route);
        $result->assertStatus(200);
        $result->assertSee('Nuevo permiso');

        $this->assertTrue($result->isOK());
    }

    public function testEditMethod()
    {
        $id    = 1;
        $route = route_to('permissions_edit_get', $id);

        $result = $this->call('get', $route);
        $result->assertStatus(200);
        $result->assertSee('Editar permiso');

        $this->assertTrue($result->isOK());
    }
}
