/*
 * Copyright (c) 2022.
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * @copyright CiHispano <administracion@cihispano.org>
 * @license For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 *
 */

// CoreUI
import '@popperjs/core'
import '@coreui/coreui'

// Simplebar
import './simplebar.min';

// Import Main styles for this application
import '../../scss/dashboard'

import Vue from 'vue'

// Permissions
Vue.component(
    'ci-permissions',
    require('../views/permissions/ListComponent.vue').default
)
Vue.component(
    'ci-permissions-add',
    require('../views/permissions/AddComponent.vue').default
)
Vue.component(
    'ci-permissions-edit',
    require('../views/permissions/EditComponent.vue').default
)

// Roles
Vue.component(
    'ci-roles',
    require('../views/roles/ListComponent.vue').default
)
Vue.component(
    'ci-roles-add',
    require('../views/roles/AddComponent.vue').default
)
Vue.component(
    'ci-roles-edit',
    require('../views/roles/EditComponent.vue').default
)

// Users
Vue.component(
    'ci-users',
    require('../views/users/ListComponent.vue').default
)
Vue.component(
    'ci-users-add',
    require('../views/users/AddComponent.vue').default
)
Vue.component(
    'ci-users-edit',
    require('../views/users/EditComponent.vue').default
)

Vue.component(
    'ci-widget-card',
    require('../components/WidgetCardComponent.vue').default
)

/**
 * [dash description]: Instanciamos Vue en 'dash'
 * @type {Vue}
 */
new Vue({
    el: '#dash',
    data: {
    	name: 'Starter-ci-boilerplate',
        version: '2.1.0'
	}
})
