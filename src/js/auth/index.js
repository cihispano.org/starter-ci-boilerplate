/*
 * Copyright (c) 2024.
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * @copyright CiHispano <administracion@cihispano.org>
 * @license For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 *
 */

// CoreUi
import '@popperjs/core'
import '@coreui/coreui'

// FontAwesome
import '@fortawesome/fontawesome-free'

// Styles for Auth views
import '../../scss/auth'

import Vue from 'vue'

// Login
Vue.component(
    'ci-login',
    require('../views/auth/LoginComponent').default
)
// Forget
Vue.component(
    'ci-forget',
    require('../views/auth/ForgetComponent').default
)
// Register
Vue.component(
    'ci-register',
    require('../views/auth/RegisterComponent').default
)

/**
 * [auth description]: Instanciamos Vue en 'auth'
 * @type {Vue}
 */
new Vue({
    el: '#auth',
    data: {
    	name: 'Starter-ci-boilerplate',
        version: '2.1.0'
	}
})
