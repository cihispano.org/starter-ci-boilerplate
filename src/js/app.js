/*
 * Copyright (c) 2024.
 * This file is part of CiHispano Starter CodeIgniter Boilerplate project.
 *
 * @copyright CiHispano <administracion@cihispano.org>
 * @license For the full copyright and license information, please view  the LICENSE file that was distributed with this source code.
 *
 */

// Bootstrap
import '@popperjs/core'
import 'bootstrap'

//import 'core-js/stable'
import Vue from 'vue'

// FontAwesome
import '@fortawesome/fontawesome-free'

// Favicon
import '../img/favicon/apple-icon-precomposed.png'
import '../img/favicon/apple-icon.png'
import '../img/favicon/apple-icon-57x57.png'
import '../img/favicon/apple-icon-60x60.png'
import '../img/favicon/apple-icon-72x72.png'
import '../img/favicon/apple-icon-76x76.png'
import '../img/favicon/apple-icon-114x114.png'
import '../img/favicon/apple-icon-120x120.png'
import '../img/favicon/apple-icon-144x144.png'
import '../img/favicon/apple-icon-152x152.png'
import '../img/favicon/apple-icon-180x180.png'
import '../img/favicon/android-icon-36x36.png'
import '../img/favicon/android-icon-48x48.png'
import '../img/favicon/android-icon-72x72.png'
import '../img/favicon/android-icon-96x96.png'
import '../img/favicon/android-icon-144x144.png'
import '../img/favicon/android-icon-192x192.png'
import '../img/favicon/favicon-16x16.png'
import '../img/favicon/favicon-32x32.png'
import '../img/favicon/favicon-96x96.png'
import '../img/favicon/ms-icon-70x70.png'
import '../img/favicon/ms-icon-144x144.png'
import '../img/favicon/ms-icon-150x150.png'
import '../img/favicon/ms-icon-310x310.png'
import '../img/favicon/favicon.ico'

// Logos
import '../img/ci-logo.svg'
import '../img/ci-logo-narrow.svg'
import '../img/cihispano-logo.png'

// App
import '../scss/app'

/**
 * [app description]: Instanciamos Vue en 'app'
 * @type {Vue}
 */
new Vue({
    el: '#app',
    data: {
    	name: 'Starter-ci-boilerplate',
        version: '2.1.0'
	}
})
